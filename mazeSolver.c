/***********************************/
/* Alexandra Valdez                */
/* September 29, 2018              */
/* CS 241 Section 001              */
/***********************************/

#include <stdio.h>

#define MAX_ROW 81
#define MAX_COL 161

char maze[MAX_ROW][MAX_COL];
int c;
int mazeWidth = 0;
int mazeHeight = 0;
int row = 1;
int col = 2;
int endX = 0;
int endY = 0;
int done = 0;
int valid = 0;

/**************************************************
* readInMaze reads a text input file char by char.
*  It checks the input for valid chars only and
*  a valid mazeWidth and mazeHeight.
*  returns 0 if the maze is valid.
*  returns 1 if the maze is invalid.
*************************************************/
int readInMaze()
{
  int i;
  int j;
  c = getchar();
  
  /* Make sure maze contains only valid characters*/
  if (c != '+' && c != '-' && c != ' ' && c != '|'
        && c != '\n')
  {
    return 1;
  }
  
  /* Store maze in 2D array */
  for (i = 0; i < MAX_ROW && c != '\n'; i++)
  {
    for (j = 0; j < MAX_COL && c != '\n'; j++)
    {
      /* Save current char to array */
      maze[i][j] = c;
      c = getchar();
      
      /* If in first row, count maze width */
      if(i == 0)
      {
        mazeWidth++;
      }
    }
    mazeHeight++;
    c = getchar();
  }
  
  c = getchar();

  if (mazeHeight != ((mazeWidth/2) + 1))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/**************************************************
* solveMaze uses a recursive algorithm and depth
*  first search strategy to solve an ASCII maze 
*  from a text input file. Checks cell directions
*  in the following order: 
*  NORTH, EAST, SOUTH, WEST.
*  It places '*'s in maze[][] to show solution.
*************************************************/
void solveMaze()
{

  if (row == 1)
  {
    maze[row][col] = '*';
  }
  
  /*Check if we won */
  if (row == endY && col == endX)
  {
    done = 1;
    return;
  }
  
  if (row - 2 >= 0)
  {
    if (maze[row - 1][col] == ' ')
    {
      row -= 1;
      maze[row][col] = '*';
      row -= 1;
      maze[row][col] = '*';
      
      if (done != 1)
      {
        solveMaze();
      }
      
      if (done != 1)
      {
        row += 1;
        maze[row][col] = ' ';
        row += 1;
      }
      else 
      {
        return;
      }
    }
  }
  
  if (col + 4 < mazeWidth)
  {
    if (maze[row][col + 2] == ' ')
    {
      col += 2;
      maze[row][col] = '*';
      col += 2;
      maze[row][col] = '*';
      
      if (done != 1)
      {
        solveMaze();
      }
      
      if (done != 1)
      {
        col -= 2;
        maze[row][col] = ' ';
        col -= 2;
      }
      else 
      {
        return;
      }
    }
  }
  
  
  if (row + 2 < mazeHeight)
  {
    if (maze[row + 1][col] == ' ')
    {
      row += 1;
      maze[row][col] = '*';
      row += 1;
      maze[row][col] = '*';
      
      if (done != 1)
      {
        solveMaze();
      }
      
      if (done != 1)
      {
        row -= 1;
        maze[row][col] = ' ';
        row -= 1;
      }
      else 
      {
        return;
      }
    }
  }
  
  if (col - 4 >= 0)
  {
    if (maze[row][col - 2] == ' ')
    {
      col -= 2;
      maze[row][col] = '*';
      col -= 2;
      maze[row][col] = '*';
      
      if (done != 1)
      {
        solveMaze();
      }
      
      if (done != 1)
      {
        col += 2;
        maze[row][col] = ' ';
        col += 2;
      }
      else 
      {
        return;
      }
    }
  }

  maze[row][col] = ' ';
}

/**************************************************
* findMazeEnd sets global variables endX and endY,
*  which correspond to the bottom right corner
*  of any given maze at any given size.
**************************************************/
void findMazeEnd()
{
  endX = mazeWidth - 3;
  endY = mazeHeight - 2;
}

/**************************************************
* printMaze loops through maze[][] and prints
*  each char, stopping at the appropriate
*  mazeHeight and mazeWidth.
**************************************************/
void printMaze()
{
  int i;
  int j;
  
  for (i = 0; i < mazeHeight; i++)
  {
    for (j = 0; j < mazeWidth; j++)
    {
      printf("%c", maze[i][j]);
    }
    printf("\n");
  }
  
  if (valid != 0)
  {
    printf("\nError\n\n");
  }
  else if (row != endY && col != endX)
  {
    printf("\nNo Solution\n\n");
  }
  else
  {
    printf("\n\n");
  }
}

int main(void)
{ 
  printf("It's running.\n");
  while (c != EOF)
  {
    valid = readInMaze();
    if (valid != 0)
    {
      printMaze();
    }
    else
    {
      findMazeEnd();
      solveMaze();
      printMaze();
    }
    
    mazeWidth = 0;
    mazeHeight = 0;
    row = 1;
    col = 2;
    done = 0;
  }
  
  return 0;
}