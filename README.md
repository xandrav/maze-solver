# ASCII Maze Solver

This program takes an ASCII maze, finds a solution, and prints it to the console. <br>
To run, download 'a.out.exe' and 'mazetest.txt', then run the following in a command prompt:<br>
./a.out < mazetest.txt